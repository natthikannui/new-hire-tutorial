PROJECT_NAME := tutorial-1
init :
	@echo "~ ~ build docker image~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) build tutorial 
	make create start
create:
	@echo "~ ~ create docker container ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) create
start : 
	@echo "~ ~ start docker container ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) start
stop : 
	@echo "~ ~ stop docker container ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) stop
restart:
	@echo "~ ~ restart docker container ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) restart 
cleanup:
	@echo "~ ~ clean up ~ ~"
	docker-compose -f docker/docker-compose.yml -p $(PROJECT_NAME) down --rmi all -v
migrate:
	@echo "~ ~ migrate ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) exec tutorial python manage.py migrate
createsuperadmin:
	@echo "~ ~ create super user for django admin ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) exec tutorial python manage.py createsuperuser
test:
	@echo "~ ~ testing ~ ~"
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) exec tutorial python manage.py test tutorial.polls
flake8:
	docker compose -f docker/docker-compose.yml -p $(PROJECT_NAME) exec tutorial flake8